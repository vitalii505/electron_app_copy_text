
import React from 'react';
import './App.css';

function App() {
  const [page1Text, setPage1Text] = React.useState('');
  const [page2Url, setPage2Url] = React.useState('');

  React.useEffect(() => {
    window.addEventListener('mouseup', handleTextSelection);
    return () => {
      window.removeEventListener('mouseup', handleTextSelection);
    };
  }, []);

  const handleTextSelection = () => {
    const selectedText = window.getSelection()?.toString();
    setPage1Text(selectedText);
  };


  const handlePage1TextChange = (event) => {
    console.log('"1--EVENT"', event);
    setPage1Text(event.target.value);
  };

  const handlePage2UrlChange = (event) => {
    setPage2Url(event.target.value);
  };

  const openPage2Url = () => {
    window.electron.ipcRenderer.sendMessage('openPage2Url', page2Url);
  };

  const handleCopyText = () => {
    const text = window.getSelection()?.toString();
    window.electron.ipcRenderer.sendMessage('copyText', text);
  };

  const handlePasteText = () => {
    navigator.clipboard.readText().then((clipText) => {
      setPage1Text(clipText);
    });
  };

  return (
    <div className='worckplace'>
      <div className='page_1'>
        <h1>Page 1</h1>
        <textarea
          className='textarea_container'
          value={page1Text}
          onChange={handlePage1TextChange}
        />
      </div>
      <div>
        <div className='page_2'>
          <h1>Page 2</h1>
          <label>URL link in browser</label>
          <input
            type="text"
            value={page2Url}
            onChange={handlePage2UrlChange}
          />
          <button className='search_button' onClick={openPage2Url}>Open</button>
        </div>
        <div>
          <button onClick={handleCopyText}>Copy</button>
          <button onClick={handlePasteText}>Paste</button>
        </div>
      </div>
    </div>
  );
}

export default App;
